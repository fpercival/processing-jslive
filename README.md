# JSLive

Library to allow dynamic control of sketches using Javascript files..



## Getting started

1. Declare and setup JSLive variable

		JSLive jslive;
	
		void setup()
		{
			size(800, 600, P2D);
		
			jslive = new JSLive(this);
		}
		
		void draw()
		{
		}
	
2. Create the config.js file.

	The config.js typically contains the javascript initialisation code. For example.
	
			fillcol = {red:100, green:100, blue:255};
			xw = 10;
			dr = 10;
			
	This file must be in the sketch folder.


	
3. Create the draw.js file.

	The draw.js contains the javascript that is run when the draw function would be executed. For example
	
			background(0);
			fill(fillcol.red, fillcol.green, fillcol.blue);
			ellipse(400, 300, 600-xw, xw);
			
			xw+=dr;
			if(xw>600 || xw<12) dr=dr*-1;
			
	This file must be in the sketch folder.




## How it works


		