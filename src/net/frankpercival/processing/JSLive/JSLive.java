package net.frankpercival.processing.JSLive;

import java.io.File;
import java.io.FileReader;

import net.frankpercival.jsrunner.JSRFileChangeListener;
import net.frankpercival.jsrunner.JSRThread;
import net.frankpercival.jsrunner.JSRunner;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ImporterTopLevel;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import processing.core.PApplet;

public class JSLive implements JSRFileChangeListener {
	
	public static String VERSION = "1.0.0";
	
	PApplet p;
	
	File setup;
	File draw;
	File config;
	
	ScriptableObject scope;
	Scriptable script;
	
	JSRunner jsr;
	
	Exception ex;

	public JSLive(PApplet applet) {
		p = applet;
		JSRThread.addListener(this);
		init();
	}
	
	protected File getFile(String wh){
		File f = p.saveFile(wh);
System.out.println(wh+" -> '"+f.getAbsolutePath()+"'");
//		if(!f.exists()) return null;
		return f;
	}
	
	protected void init(){
		setup = getFile("setup.js");
		config = getFile("config.js");
		draw = getFile("draw.js");
		
		scope = new ImporterTopLevel(Context.enter());
		script = Context.toObject(p, scope);
		ScriptableObject.putProperty(scope, "JSLIVE",  Context.javaToJS(this, script));
		ScriptableObject.putProperty(scope, "P",  Context.javaToJS(p, script));
		exec("function load(f, fn){ if(fn)JSLIVE.load(f, fn); else JSLIVE.load(f); }");
		exec("function execFile(f, fn){ if(fn)JSLIVE.execFile(f, fn); else JSLIVE.execFile(f); }");
		exec("function col(c1, c2, c3, c4){ if(c4) return JSLIVE.color(c1, c2, c3, c4); else return JSLIVE.color(c1, c2, c3); }");

		jsr = new JSRunner();
		jsr.setScope(scope);
		jsr.addFile("setup", setup.getAbsolutePath());
		if(setup!=null) {
			jsr.runHost(script, "setup", "JSLIVE", this);
		}
		jsr.addFile("config", config.getAbsolutePath());
		if(config!=null) {
			jsr.runHost(script, "config", "JSLIVE", this);
		}
		jsr.addFile("draw", draw.getAbsolutePath());

		p.registerMethod("draw", this);
	}
	
	public void draw() {
		try
		{
			jsr.runHost(script, "draw", "JSLIVE", this);
		} catch(Exception e) {
			if(ex==null || !ex.getMessage().equals(e.getMessage())){
				e.printStackTrace();
				ex=e;
			}
		}
	}
	public Object load(String fn) {
		return load(fn, (new File(fn)).getName());// "JSLive load");
	}
	
	public Object load(String fn, String sn) {
		File f = new File(fn);
		Object r = jsr.addFile(fn, sn);
		jsr.runHost(script, sn, "JSLIVE", this);
		return r;
	}
	
	public Object execFile(String fn){
		return execFile(fn, "JSLive file");
	}
	
	public Object execFile(String fn, String sn){
		Context c = Context.enter();
		Object r=null;
		try {
			FileReader fr = new FileReader(fn);
			c.evaluateReader(scope, fr, sn, 1, null);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return r;
	}
	
	public Object exec(String s){
		return exec(s, "JSLive exec");
	}
	
	public Object exec(String s, String sn) {
		Context c = Context.enter();
		Object r=null;
		r = c.evaluateString(scope, s, sn, 1, null);
		return r;
	}

	@Override
	public void fileChanged(String fname) {
		System.out.println("changed "+fname);
		if(fname.equalsIgnoreCase("draw")) {
			ex=null;
			return;
		}
		if(fname.equalsIgnoreCase("startup")) {
			return;
		}
		jsr.runHost(script, fname, "JSLIVE", this);
/*
*/
	}

	@Override
	public void fileDeleted(String fname) {
		System.out.println("Removed file "+fname);
	}

	public final int color(int v1, int v2, int v3, int alpha) {
		return p.color(v1, v2, v3, alpha);
	}

	public final int color(int v1, int v2, int v3) {
		return p.color(v1, v2, v3);
	}

}
